import AnimalDetection from "@/app/ml5-poc/AnimalDetection";

const Home = () => {
  return (
      <div>
        <script src="https://unpkg.com/ml5@latest/dist/ml5.min.js"></script>
        <h1>Your Next.js Object Detection App</h1>
        <AnimalDetection />
      </div>
  );
};

export default Home;
