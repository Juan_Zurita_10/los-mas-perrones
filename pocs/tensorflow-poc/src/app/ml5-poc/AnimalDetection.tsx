"use client"

import {useEffect} from 'react';

const AnimalDetection = () => {
    let objectDetector: { detect: (arg0: any, arg1: (err: any, results: any) => void) => void; };
    let video: HTMLVideoElement;
    let canvas, ctx: CanvasRenderingContext2D | null;
    const width = 480;
    const height = 360;

    const make = async () => {
        video = await getVideo();
        // @ts-ignore
        objectDetector = await ml5.objectDetector('cocossd', startDetecting);
        canvas = createCanvas(width, height);
        ctx = canvas.getContext('2d');
    };

    useEffect(() => {
        make();

        return () => {
            if (video) {
                // @ts-ignore
                video.srcObject.getTracks().forEach((track) => track.stop());
            }
        };
    }, []);

    const startDetecting = () => {
        console.log('model ready');
        detect();
    };

    const detect = () => {
        objectDetector.detect(video, (err, results) => {
            if (err) {
                console.log(err);
                return;
            }

            const objects = results.filter((object: { label: string; }) =>
                ['dog', 'cat', 'cow', 'horse'].includes(object.label.toLowerCase()),
            );

            if (objects) {
                draw(objects);
            }
            detect();
        });
    };

    const draw = (objects:[]) => {
        // @ts-ignore
        ctx.fillStyle = '#000000';
        // @ts-ignore
        ctx.fillRect(0, 0, width, height);
// @ts-ignore
        ctx.drawImage(video, 0, 0);
        for (let i = 0; i < objects.length; i += 1) {
            const animal = objects[i]
            // @ts-ignore
            ctx.font = '16px Arial';
            // @ts-ignore
            ctx.fillStyle = 'green';
            // @ts-ignore
            ctx.fillText(animal.label,animal.x + 4, animal.y + 16);
            // @ts-ignore
            ctx.beginPath();
            // @ts-ignore
            ctx.rect(animal.x, animal.y, animal.width, animal.height);
            // @ts-ignore
            ctx.strokeStyle = 'green';
            // @ts-ignore
            ctx.stroke();
            // @ts-ignore
            ctx.closePath();

            // @ts-ignore
            const animalModel = {class: animal.label, score: animal.confidence,
                screenshot: getCameraId(),
                uniqueCode: generateUniqueCode(),
                detectionTime: new Date().toLocaleString(),
            };

            console.log(animalModel)
        }
    };

    const getVideo = async () => {
        const videoElement = document.createElement('video');
        videoElement.setAttribute('style', 'display: none;');
        videoElement.width = width;
        videoElement.height = height;
        document.body.appendChild(videoElement);
        videoElement.srcObject = await navigator.mediaDevices.getUserMedia({video: true});
        await videoElement.play();

        return videoElement;
    };

    const createCanvas = (w: number, h: number) => {
        const canvas = document.createElement('canvas');
        canvas.width = w;
        canvas.height = h;
        document.body.appendChild(canvas);
        return canvas;
    };

    const getCameraId = () => {
        if (video && video.srcObject) {
            const videoTracks = (video.srcObject as MediaStream).getVideoTracks();
            if (videoTracks.length > 0) {
                const videoTrack = videoTracks[0];
                const settings = videoTrack.getSettings();
                if (settings.deviceId) {
                    return settings.deviceId;
                }
            }
        }
        return 'unknownCamera';
    };

    const generateUniqueCode = () => {
        return Math.random().toString(36).substring(2, 11);
    };

    return <div></div>;
};

export default AnimalDetection;