```markdown
### Code Snippet Documentation

```javascript
require('dotenv').config();

const accountSid = process.env.TWILIO_ACCOUNT_SID;
const authToken = process.env.TWILIO_AUTH_TOKEN;
const client = require('twilio')(accountSid, authToken);

client.messages
    .create({
        body: 'Test Message',
        from: '+12678416036',
        to: '+59165712596'
    })
    .then(message => console.log(message.sid))
    .catch(err => console.error(err));
```

This code snippet showcases the integration of Twilio for sending an SMS notification using Node.js.

Prerequisites

Node.js installed
Twilio account
.env file with the following variables:
TWILIO_ACCOUNT_SID=ACa0fc095adcdc01701103b1cdb511d7b6
TWILIO_AUTH_TOKEN=ec070bd20f11b4a1c32930f0e1916a8c
Description

Loads environment variables using dotenv.
Retrieves Twilio Account SID and Auth Token from the environment variables.
Initializes the Twilio client with the obtained credentials.
Sends a test message with the body "Test Message" from the Twilio number +12678416036 to the recipient number +59165712596.
Logs the SID of the sent message upon successful transmission.
Steps to Run

Install required dependencies:
npm install twilio dotenv
reate a .env file in the project root directory and add the Twilio credentials.
Save the code snippet in a JavaScript file (e.g., sms-notification.js).
Run the code
node poc.js