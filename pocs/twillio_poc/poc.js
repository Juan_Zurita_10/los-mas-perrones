require('dotenv').config();

const accountSid = process.env.TWILIO_ACCOUNT_SID;
const authToken = process.env.TWILIO_AUTH_TOKEN;
const client = require('twilio')(accountSid, authToken);

client.messages
    .create({
        body: 'Test Message',
        from: '+12678416036',
        to: '+59165712596'
    })
    .then(message => console.log(message.sid))
    .catch(err => console.error(err));