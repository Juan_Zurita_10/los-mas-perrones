import apiService from './ApiService';
const API_BASE_URL = 'http://localhost:3002';

export async function insertAnimal(animal: unknown) {
  const endpoint = 'petregister';
  const service = apiService(API_BASE_URL, endpoint);
  const asw = await service.post(animal);
  return asw;
}
