import apiService from './ApiService';
const API_BASE_URL = 'http://localhost:3002/sms';

export async function sendSMS(message : string) {
  const endpoint = 'sendRepNum';
  const service = apiService(API_BASE_URL, endpoint);
  
  try {
    const response = await service.post({message});
    return response;
  } catch (error) {
    console.error('Error sending SMS:', error);
    throw error;
  }
}
