import apiService from './ApiService';
const API_BASE_URL = 'http://localhost:3002/jetmail';

export async function sendMail(message : string) {
  const endpoint = 'sendemail';
  const service = apiService(API_BASE_URL, endpoint);
  const emailData = {
    textPart: message
  };
  try {
    const response = await service.post(emailData);
    return response;
  } catch (error) {
    console.error('Error sending SMS:', error);
    throw error;
  }
}
