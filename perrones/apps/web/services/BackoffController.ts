import { AxiosResponse } from 'axios';

type AxiosFunction<T = unknown> = () => Promise<AxiosResponse<T>>;

const exponentialBackoff = async <T>(
  axiosFunction: AxiosFunction<T>,
  maxRetries: number,
): Promise<T | undefined> => {
  const ERROR_MESSAGE: string =
    'Error: An attempt was made to connect with the service and the number of x attempts failed.\n';
  const WAIT_TIME = 2;
  const SECOND = 100;
  let retries = maxRetries;

  while (retries > 0) {
    try {
      const response = await axiosFunction();
      return response.data;
    } catch (error) {
      retries--;
      if (retries > 0) {
        const delay = Math.pow(WAIT_TIME, maxRetries - retries) * SECOND;
        console.log('delay: ', delay / 1000);
        await new Promise((resolve) => setTimeout(resolve, delay));
      } else {
        if (!(typeof window === 'undefined' || !window.alert)) {
          alert(ERROR_MESSAGE);
        }
        throw error;
      }
    }
  }

  return undefined;
};

export default exponentialBackoff;
