import apiService from './ApiService';
const API_BASE_URL = 'http://localhost:3002';

export async function getAllPets() {
  const endpoint = 'petregister';
  const service = apiService(API_BASE_URL, endpoint);
  const asw = await service.getAll();
  return asw;
}
