// apiService.js
import axios from 'axios';
import exponentialBackoff from './BackoffController';

const apiService = (url: string, endpoint: string) => {
  const jsonContentHeader = {
    'Content-type': 'application/json; charset=UTF-8',
  };
  const baseUrl = `${url}/${endpoint}`;

  const MAX_RETRIES = 3;

  return {
    getAll: async () =>
      await exponentialBackoff(() => axios.get(baseUrl), MAX_RETRIES),

    getById: async (id: unknown) =>
      await exponentialBackoff(
        () => axios.get(`${baseUrl}/${id}`),
        MAX_RETRIES,
      ),

    post: async (body: unknown) =>
      await exponentialBackoff(
        () => axios.post(baseUrl, body, { headers: jsonContentHeader }),
        MAX_RETRIES,
      ),

    put: async (id: unknown, body: unknown) =>
      await exponentialBackoff(
        () =>
          axios.put(`${baseUrl}/${id}`, body, { headers: jsonContentHeader }),
        MAX_RETRIES,
      ),

    patch: async (id: unknown, body: unknown) =>
      await exponentialBackoff(
        () =>
          axios.patch(`${baseUrl}/${id}`, body, { headers: jsonContentHeader }),
        MAX_RETRIES,
      ),

    delete: async (id: unknown) =>
      await exponentialBackoff(
        () => axios.delete(`${baseUrl}/${id}`),
        MAX_RETRIES,
      ),
  };
};

export default apiService;
