import NextAuth from 'next-auth/next';
import KeycloakProvider from 'next-auth/providers/keycloak';
import axios from 'axios';
import { Account, Session } from 'next-auth';
import { JWT } from 'next-auth/jwt';

async function keycloakSignOut({ jwt }: { jwt: JWT }) {
  const { provider, id_token } = jwt;

  if (provider === 'keycloak') {
    try {
      const params = new URLSearchParams();
      params.append('id_token_hint', id_token);
      await axios.get(
        `${
          process.env.END_SESSION_URL
        }?id_token_hint=${id_token}&post_logout_redirect_uri=${encodeURIComponent(
          process.env.NEXTAUTH_URL!,
        )}`,
      );
    } catch (error) {
      return new Response(null, { status: 500 });
    }
    return new Response(null, { status: 200 });
  }
}

const decode = function (token: string) {
  const tokenPart = token.split('.')[1];
  const buffer = Buffer.from(String(tokenPart), 'base64');
  return JSON.parse(buffer.toString());
};

export const authOptions = {
  providers: [
    KeycloakProvider({
      clientId: process.env.CLIENT_ID!,
      clientSecret: process.env.CLIENT_SECRET!,
      issuer: process.env.AUTH_ISSUER,
    }),
  ],
  callbacks: {
    async jwt({ token, account }: { token: JWT; account: Account | null }) {
      if (account) {
        token.provider = account.provider;
        token.id_token = account.id_token;
        token.roles = decode(account.access_token!).realm_access.roles;
      }

      return token;
    },
    async session({ session, token }: { session: Session; token: JWT }) {
      if (session.user) {
        session.user.roles = token.roles;
      }
      return session;
    },
  },
  events: {
    async signOut({ token }: { token: JWT }) {
      await keycloakSignOut({ jwt: token });
    },
  },
};

const handler = NextAuth(authOptions);

export { handler as GET, handler as POST };
