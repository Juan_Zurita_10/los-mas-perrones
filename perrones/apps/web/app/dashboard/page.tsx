'use client';

import React from 'react';
import Header from '../../common/header/Header';
import LoadIfLoginWrapper from '../../common/loadIfLogin/LoadIfLoginWrapper';
import { Typography, ThemeProvider, Box } from '@mui/material';
import customTheme from '../../types/DefaultTheme';
import Charts from '../../common/charts/Charts';
import DataTable from '../../common/table/DataTable';
import SendInformationButton from '../../components/SendInformationButton';

const Dashboard = () => (
  <LoadIfLoginWrapper role={['veterinarian', 'cityPlanner']}>
    <div
      style={{
        position: 'relative',
        backgroundColor: '#f0f0f0',
        color: '#333',
        margin: 0,
        padding: 0,
        height: '90vh',
        paddingBottom: '60px',
      }}
    >
      <Header />
      <ThemeProvider theme={customTheme}>
        <Box display="flex" justifyContent="space-between" alignItems="center">
          <Typography paddingTop={3} paddingLeft={3} variant="h4" gutterBottom>
            Dashboard 2023
          </Typography>
          <Box padding={3}>
            <SendInformationButton />
          </Box>
        </Box>

        <Charts></Charts>
        <DataTable></DataTable>
      </ThemeProvider>
    </div>
  </LoadIfLoginWrapper>
);

export default Dashboard;
