'use client';
import React from 'react';
import {
  Button,
  Typography,
  Container,
  Box,
  CssBaseline,
  ThemeProvider,
} from '@mui/material';
import { signIn } from 'next-auth/react';
import customTheme from '../types/DefaultTheme';

const LandingPage: React.FC = () => {
  const titleStyle: React.CSSProperties = {
    position: 'absolute',
    top: '80px',
    left: '120px',
    fontFamily: 'Roboto',
    fontWeight: 'bold',
    fontSize: '128px',
    color: '#ffffff',
    textShadow: '1px 1px 0 #1E4620',
  };

  const descriptionStyle: React.CSSProperties = {
    position: 'absolute',
    top: '230px',
    left: '120px',
    fontFamily: 'Roboto',
    fontSize: '24px',
    color: '#ffffff',
    textShadow: '1px 1px 0 #1E4620',
  };

  const buttonContainerStyle: React.CSSProperties = {
    position: 'absolute',
    top: '300px',
    left: '120px',
  };

  const buttonStyle: React.CSSProperties = {
    fontSize: '1.5rem',
  };

  return (
    <ThemeProvider theme={customTheme}>
      <React.Fragment>
        <CssBaseline />
        <div
          style={{
            backgroundImage:
              'url(https://image10.photobiz.com/8495/28_20220226150052_6422823_xlarge.jpg)',
            backgroundSize: 'cover',
            height: '100vh',
            position: 'relative',
          }}
        >
          <Container
            style={{
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
              height: '100%',
              padding: '2rem',
            }}
          >
            <Box textAlign="center">
              <Typography
                variant="h2"
                color="primary"
                gutterBottom
                style={titleStyle}
              >
                Pet Patrol
              </Typography>
              <Typography variant="body1" style={descriptionStyle}>
                Find your ideal pet business location!
              </Typography>
              <div style={buttonContainerStyle}>
                <Button
                  variant="contained"
                  color="primary"
                  style={buttonStyle}
                  onClick={() => {
                    signIn('keycloak', {
                      callbackUrl: 'http://localhost:3000/information',
                      redirect: true,
                    });
                  }}
                >
                  Get Started
                </Button>
              </div>
            </Box>
          </Container>
        </div>
      </React.Fragment>
    </ThemeProvider>
  );
};

export default LandingPage;
