import React from 'react';
import Header from '../../common/header/Header';
import LoadIfLoginWrapper from '../../common/loadIfLogin/LoadIfLoginWrapper';
import AnimalDetection from '../../common/camera/AnimalDetection';
import './styles.css';

const App = () => (
  <LoadIfLoginWrapper role={['veterinarian', 'cameraMan']}>
    <div style={{ height: '100%' }}>
      <Header />
      <div
        style={{ display: 'flex', alignItems: 'center', height: '92.9vh' }}
        id="animal-detection-container"
      >
        <AnimalDetection></AnimalDetection>
      </div>
    </div>
  </LoadIfLoginWrapper>
);

export default App;
