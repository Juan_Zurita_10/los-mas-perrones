import React from 'react';
import { Box, Paper, Typography } from '@mui/material';
import Header from '../../common/header/Header';
import LoadIfLoginWrapper from '../../common/loadIfLogin/LoadIfLoginWrapper';

const InformationWindow: React.FC = () => {
  const containerStyle: React.CSSProperties = {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '98vh',
  };

  const paperStyle: React.CSSProperties = {
    padding: 3,
    maxWidth: '70%',
    textAlign: 'left',
    backgroundColor: '#cb8267',
    borderRadius: 8,
    boxShadow: '0px 2px 4px rgba(0, 0, 0, 0.1)',
  };

  const titleStyle: React.CSSProperties = {
    position: 'relative',
    fontFamily: 'Roboto',
    fontWeight: 'bold',
    fontSize: '128px',
    color: 'black',
  };

  const roleStyle: React.CSSProperties = {
    fontWeight: 'bold',
    fontStyle: 'italic',
    color: '#fff',
  };

  return (
    <LoadIfLoginWrapper>
      <div style={containerStyle}>
        <Paper sx={paperStyle}>
          <Header />
          <Box display="flex" justifyContent="center">
            <Typography
              variant="h2"
              color="primary"
              style={titleStyle}
              paddingBottom={5}
            >
              Pet Patrol
            </Typography>
          </Box>
          <Typography variant="body1" marginBottom={2} fontSize={20}>
            This application continuously monitors the influx of pets,
            specifically <b>cats and dogs</b>, and stores this data for metric
            display purposes.
          </Typography>
          <Typography variant="body1" marginBottom={2} fontSize={20}>
            <b>Data Types:</b> Dogs, Cats, and Others.
            <br />
            <b>&apos;Others&apos;</b> include animals not necessarily considered
            pets, such as horses, cows, elephants, etc.
          </Typography>
          <Typography variant="body1" marginBottom={2} fontSize={20}>
            Data is saved with a{' '}
            <b>reliability percentage between 70% and 100%</b>. Messages are
            sent to record how many pets have passed through the area.
          </Typography>
          <Typography variant="body1" marginBottom={2} fontSize={20}>
            The application is accessible to users with different roles:
            <ul>
              <li style={roleStyle}>
                Camera Man: Access to the view of the camera.
              </li>
              <li style={roleStyle}>
                City Planner: Access to collected metrics from the camera on the
                dashboards.
              </li>
              <li style={roleStyle}>
                Veterinarian: Access to metrics and the dashboard.
              </li>
            </ul>
            The purpose of the application is to showcase the{' '}
            <b>
              profitability of establishing a veterinary clinic or pet store
            </b>{' '}
            in a specific area or to address the <b>area&apos;s needs</b>{' '}
            accordingly.
          </Typography>
          <Typography variant="caption">
            &copy; &quot;Perrones&quot; 2023. All Rights Reserved.
          </Typography>
        </Paper>
      </div>
    </LoadIfLoginWrapper>
  );
};

export default InformationWindow;
