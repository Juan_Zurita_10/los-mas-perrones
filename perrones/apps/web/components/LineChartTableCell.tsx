import { TableCell, Typography } from '@mui/material';
import React from 'react';

const LineChartTableCell: React.FC = () => (
  <TableCell>
    <Typography>
      Shows the total quantity of Cats and Dogs <br /> based on the current
      year&rsquo;s months.
    </Typography>
  </TableCell>
);

export default LineChartTableCell;
