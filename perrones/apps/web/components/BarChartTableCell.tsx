import { TableCell, Typography } from '@mui/material';
import React from 'react';

const BarChartTableCell: React.FC = () => (
  <TableCell>
    <Typography>
      <strong>Left Bar (green):</strong> displays the total number of animals.
    </Typography>
    <Typography>
      <strong> Middle Bar (blue):</strong> displays those with over 80%
      reliability.
    </Typography>
    <Typography>
      <strong>Right Bar (pink):</strong> displays those with reliability between
      70% and 80%.
    </Typography>
  </TableCell>
);

export default BarChartTableCell;
