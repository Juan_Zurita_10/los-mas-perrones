import * as React from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import { Divider, Typography } from '@mui/material';
import AnimalModel from '../types/AnimalData';

export default function CustomList({ items }: { items: AnimalModel[] }) {
  function capitalize(str: string) {
    const firstChar = str.charAt(0).toUpperCase();
    const resOfString = str.slice(1);
    return firstChar + resOfString;
  }

  return (
    <List
      sx={{
        width: '100%',
        maxWidth: 500,
        bgcolor: 'background.paper',
        position: 'relative',
        overflow: 'auto',
        maxHeight: '91vh',
        '& ul': { padding: 0 },
      }}
      subheader={<li />}
    >
      {items.map((item, sectionId) => (
        <ListItem key={`item-${sectionId}-${item}`}>
          <ListItemText
            primary={`${capitalize(item.typeAnimal)}`}
            secondary={
              <React.Fragment>
                <Typography
                  sx={{ display: 'inline' }}
                  component="span"
                  variant="body2"
                  color="text.primary"
                >
                  {item.detectionTime.toLocaleString()}
                </Typography>
              </React.Fragment>
            }
          />
          <Divider variant="inset" component="li" />
        </ListItem>
      ))}
    </List>
  );
}
