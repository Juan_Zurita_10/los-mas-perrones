import { TableCell, Typography } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { getAllPets } from '../services/DashboardService';
import { INVALID_DATA_ERROR } from '../common/dashboardConstants/exceptionDashboardConstants';
import { FETCH_ERROR } from '../common/dashboardConstants/exceptionDashboardConstants';

const PetsCountTableCell: React.FC = () => {
  const [petsCount, setPetsCount] = useState({
    cats: 0,
    dogs: 0,
    others: 0,
    total: 0,
  });

  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const jsonData = await getAllPets();

        if (Array.isArray(jsonData.registerData)) {
          let cats = 0;
          let dogs = 0;
          let others = 0;

          jsonData.registerData.forEach((register: { typeAnimal: string }) => {
            switch (register.typeAnimal) {
              case 'cat':
                cats++;
                break;
              case 'dog':
                dogs++;
                break;
              case 'others':
                others++;
                break;
              default:
                break;
            }
          });

          const total = cats + dogs;

          setPetsCount({ cats, dogs, others, total });
          setLoaded(true);
        } else {
          throw new Error(INVALID_DATA_ERROR);
        }
      } catch (error) {
        throw new Error(`${FETCH_ERROR} ${error}`);
      }
    };

    if (!loaded) {
      fetchData();
    }
  }, [loaded]);

  return (
    <TableCell>
      <Typography>
        <strong>Total Cats:</strong> {petsCount.cats}
      </Typography>
      <Typography>
        <strong>Total Dogs:</strong> {petsCount.dogs}
      </Typography>
      <Typography>
        <strong>Total Others:</strong> {petsCount.others}
      </Typography>
      <Typography>
        <strong>Total Cats & Dogs:</strong> {petsCount.total}
      </Typography>
    </TableCell>
  );
};

export default PetsCountTableCell;
