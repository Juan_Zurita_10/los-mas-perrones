import React, { useEffect, useState } from 'react';
import { BarChart } from '@mui/x-charts/BarChart';
import { getAllPets } from '../services/DashboardService';
import { INVALID_DATA_ERROR } from '../common/dashboardConstants/exceptionDashboardConstants';
import { FETCH_ERROR } from '../common/dashboardConstants/exceptionDashboardConstants';
import { DOG_POSITION } from '../common/dashboardConstants/dashboardPositionsConstants';
import { CAT_POSITION } from '../common/dashboardConstants/dashboardPositionsConstants';
import { OTHERS_POSITION } from '../common/dashboardConstants/dashboardPositionsConstants';

type PetRegister = {
  typeAnimal: string;
  score: number;
};

type BarSeriesData = {
  seriesIndex: number;
  index: number;
  data: number[];
}[];

const BarChartComponent: React.FC = () => {
  const initialChartData: BarSeriesData = [
    { seriesIndex: 0, index: 0, data: [0, 0, 0] },
    { seriesIndex: 1, index: 1, data: [0, 0, 0] },
    { seriesIndex: 2, index: 2, data: [0, 0, 0] },
  ];

  const [chartData, setChartData] = useState<BarSeriesData>(initialChartData);
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const jsonData = await getAllPets();

        if (Array.isArray(jsonData.registerData)) {
          const updatedChartData = JSON.parse(
            JSON.stringify(initialChartData),
          ) as BarSeriesData;

          jsonData.registerData.forEach((register: PetRegister) => {
            switch (register.typeAnimal) {
              case 'cat':
                if (
                  updatedChartData &&
                  updatedChartData[0] &&
                  updatedChartData[0].data
                ) {
                  updatedChartData[0].data[CAT_POSITION]++;
                  if (
                    register.score > 0.8 &&
                    updatedChartData &&
                    updatedChartData[1] &&
                    updatedChartData[1].data
                  ) {
                    updatedChartData[1].data[CAT_POSITION]++;
                  } else {
                    if (
                      updatedChartData &&
                      updatedChartData[2] &&
                      updatedChartData[2].data
                    ) {
                      updatedChartData[2].data[CAT_POSITION]++;
                    }
                  }
                }
                break;
              case 'dog':
                if (
                  updatedChartData &&
                  updatedChartData[0] &&
                  updatedChartData[0].data
                ) {
                  updatedChartData[0].data[DOG_POSITION]++;
                  if (
                    register.score > 0.8 &&
                    updatedChartData &&
                    updatedChartData[1] &&
                    updatedChartData[1].data
                  ) {
                    updatedChartData[1].data[DOG_POSITION]++;
                  } else {
                    if (
                      updatedChartData &&
                      updatedChartData[2] &&
                      updatedChartData[2].data
                    ) {
                      updatedChartData[2].data[DOG_POSITION]++;
                    }
                  }
                }
                break;
              case 'others':
                if (
                  updatedChartData &&
                  updatedChartData[0] &&
                  updatedChartData[0].data
                ) {
                  updatedChartData[0].data[OTHERS_POSITION]++;
                  if (
                    register.score > 0.8 &&
                    updatedChartData &&
                    updatedChartData[1] &&
                    updatedChartData[1].data
                  ) {
                    updatedChartData[1].data[OTHERS_POSITION]++;
                  } else {
                    if (
                      updatedChartData &&
                      updatedChartData[2] &&
                      updatedChartData[2].data
                    ) {
                      updatedChartData[2].data[OTHERS_POSITION]++;
                    }
                  }
                }
                break;
              default:
                break;
            }
          });
          setChartData(updatedChartData);
        } else {
          throw new Error(INVALID_DATA_ERROR);
        }
      } catch (error) {
        throw new Error(`${FETCH_ERROR} ${error}`);
      } finally {
        setLoading(false);
      }
    };

    fetchData();
  }, []);

  const formattedData = chartData.map((series) => ({
    name: `Series ${series.seriesIndex + 1}`,
    data: series.data,
  }));

  return (
    <div
      style={{
        height: '100%',
        display: 'flex',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <BarChart
        xAxis={[{ scaleType: 'band', data: ['Cats', 'Dogs', 'Others'] }]}
        series={formattedData}
        width={440}
        height={400}
        margin={{ top: 20, right: 20, bottom: 20, left: 20 }}
      />
    </div>
  );
};

export default BarChartComponent;
