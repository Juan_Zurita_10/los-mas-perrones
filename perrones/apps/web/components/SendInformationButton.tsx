import React, { useState } from 'react';
import { Button } from '@mui/material';
import { getAllPets } from '../services/DashboardService';
import { sendSMS } from '../services/NotificationService';
import {sendMail} from '../services/MailNotificationService';

const MAX_RETRY_ATTEMPTS = 3;
const INTERVAL_IN_MS = 6000000; 

const SendInformationButton: React.FC = () => {
  const [retryCount, setRetryCount] = useState(0);
  const [intervalId, setIntervalId] = useState<NodeJS.Timeout | null>(null);

  const handleButtonClick = async () => {
    try {
      const jsonData = await getAllPets();

      if (!intervalId) {
        const id = setInterval(async () => {
          await handleButtonClick();
        }, INTERVAL_IN_MS);
        setIntervalId(id);
      }

      if (Array.isArray(jsonData.registerData)) {
        let cats = 0;
        let dogs = 0;
        let others = 0;

        jsonData.registerData.forEach((register: { typeAnimal: any; }) => {
          switch (register.typeAnimal) {
            case 'cat':
              cats++;
              break;
            case 'dog':
              dogs++;
              break;
            case 'others':
              others++;
              break;
            default:
              break;
          }
        });

        const totalCountString = `Total Cats: ${cats},\n Total Dogs: ${dogs},\n Total Others: ${others}`;
        const message = `Pet Counts:\n${totalCountString}`;
        const response = await retrySendSMS(message);
        const mailResponse = await sendMail(message)
        console.log(response)
        console.log(mailResponse)
      } else {
        console.error('Invalid data format');
      }
    } catch (error) {
      console.error('Error fetching pets:', error);
      clearInterval(intervalId as NodeJS.Timeout); 
      setIntervalId(null); 
    }
  };

  const retrySendSMS = async (message: string): Promise<any> => {
    try {
      const response = await sendSMS(message);
      console.log(response);
      return response;
    } catch (error) {
      if (retryCount < MAX_RETRY_ATTEMPTS) {
        setRetryCount(retryCount + 1);
        console.log(`Retrying... Attempt ${retryCount + 1}`);
        return retrySendSMS(message);
      } else {
        console.error('Max retry attempts reached');
        throw new Error('Max retry attempts reached');
      }
    }
  };

  return (
    <Button variant="contained" color="primary" onClick={handleButtonClick}>
      Send Report-SMS && Mail
    </Button>
  );
};

export default SendInformationButton;
