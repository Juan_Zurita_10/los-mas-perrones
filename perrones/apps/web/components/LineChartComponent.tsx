'use client';

import React, { useState, useEffect } from 'react';
import { getAllPets } from '../services/DashboardService';
import { FETCH_ERROR } from '../common/dashboardConstants/exceptionDashboardConstants';
import dynamic from 'next/dynamic';

const ReactApexChart = dynamic(
  () => import('react-apexcharts').then((module) => module.default),
  {
    ssr: false,
  },
);

type LineChartData = {
  seriesIndex: number;
  data: number[];
}[];

interface ChartOptions {
  chart: {
    height: number;
    id: string;
    zoom: {
      enabled: boolean;
    };
  };
  dataLabels: {
    enabled: boolean;
  };
  xaxis: {
    categories: string[];
  };
}

interface ChartData {
  name: string;
  data: number[];
}

interface ChartState {
  options: ChartOptions;
  series: ChartData[];
}

interface PetData {
  _id: string;
  typeAnimal: string;
  detectionTime: string;
}

const LineChartComponent: React.FC = () => {
  const [chartData, setChartData] = useState<ChartState>({
    options: {
      chart: {
        height: 350,
        id: 'basic-bar',
        zoom: {
          enabled: false,
        },
      },
      dataLabels: {
        enabled: false,
      },
      xaxis: {
        categories: [
          'Jan',
          'Feb',
          'Mar',
          'Apr',
          'May',
          'Jun',
          'Jul',
          'Aug',
          'Sep',
          'Oct',
          'Nov',
          'Dec',
        ],
      },
    },
    series: [],
  });

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await getAllPets();
        if (response && response.registerData) {
          const petData: PetData[] = response.registerData.filter(
            (pet: PetData) =>
              pet.typeAnimal === 'cat' || pet.typeAnimal === 'dog',
          );

          const catMonthlyCounts: number[] = Array(12).fill(0);
          const dogMonthlyCounts: number[] = Array(12).fill(0);

          petData.forEach((pet: PetData) => {
            const detectionDate = new Date(pet.detectionTime);
            const month = detectionDate.getMonth();
            if (pet.typeAnimal === 'cat') {
              catMonthlyCounts[month]++;
            } else if (pet.typeAnimal === 'dog') {
              dogMonthlyCounts[month]++;
            }
          });

          const updatedChartData: LineChartData = [
            {
              seriesIndex: 0,
              data: catMonthlyCounts,
            },
            {
              seriesIndex: 1,
              data: dogMonthlyCounts,
            },
          ];

          setChartData((prevData) => ({
            ...prevData,
            series: updatedChartData.map((item, index) => ({
              name: index === 0 ? 'Cats' : 'Dogs',
              data: item.data,
            })),
          }));
        }
      } catch (error) {
        throw new Error(`${FETCH_ERROR} ${error}`);
      }
    };

    fetchData();
  }, []);

  return (
    <div className="app" style={{ height: 400 }}>
      <div className="row">
        <div className="mixed-chart">
          {typeof window !== 'undefined' && (
            <ReactApexChart
              options={chartData.options}
              series={chartData.series}
              type="line"
              height={385}
              width={'100%'}
              margin={{ top: 20, right: 20, bottom: 20, left: 20 }}
            />
          )}
        </div>
      </div>
    </div>
  );
};

export default LineChartComponent;
