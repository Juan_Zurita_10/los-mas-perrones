import { TableCell, Typography } from '@mui/material';
import React from 'react';

const PieChartTableCell: React.FC = () => (
  <TableCell>
    <Typography>
      Shows totals including the range of 70% to 100% <br />
      of Cats, Dogs, and Others.
    </Typography>
  </TableCell>
);

export default PieChartTableCell;
