import React, { useEffect, useState } from 'react';
import { PieChart } from '@mui/x-charts/PieChart';
import { getAllPets } from '../services/DashboardService';
import { INVALID_DATA_ERROR } from '../common/dashboardConstants/exceptionDashboardConstants';
import { FETCH_ERROR } from '../common/dashboardConstants/exceptionDashboardConstants';
import { DOG_POSITION } from '../common/dashboardConstants/dashboardPositionsConstants';
import { CAT_POSITION } from '../common/dashboardConstants/dashboardPositionsConstants';
import { OTHERS_POSITION } from '../common/dashboardConstants/dashboardPositionsConstants';

const PieChartComponent: React.FC = () => {
  const [pieChartData, setPieChartData] = useState([
    {
      seriesIndex: 0,
      index: 0,
      data: [
        { id: 0, value: 0, label: 'Cats' },
        { id: 1, value: 0, label: 'Dogs' },
        { id: 2, value: 0, label: 'Others' },
      ],
    },
  ]);

  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const jsonData = await getAllPets();

        if (Array.isArray(jsonData.registerData)) {
          const updatedPieChartData = JSON.parse(JSON.stringify(pieChartData));

          jsonData.registerData.forEach((register: { typeAnimal: string }) => {
            switch (register.typeAnimal) {
              case 'cat':
                updatedPieChartData[0].data[CAT_POSITION].value++;
                break;
              case 'dog':
                updatedPieChartData[0].data[DOG_POSITION].value++;
                break;
              case 'others':
                updatedPieChartData[0].data[OTHERS_POSITION].value++;
                break;
              default:
                break;
            }
          });

          setPieChartData(updatedPieChartData);
          setLoaded(true);
        } else {
          throw new Error(INVALID_DATA_ERROR);
        }
      } catch (error) {
        throw new Error(`${FETCH_ERROR} ${error}`);
      }
    };

    if (!loaded) {
      fetchData();
    }
  }, [loaded, pieChartData]);

  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
      }}
    >
      <PieChart
        series={pieChartData}
        width={500}
        height={400}
        margin={{ top: 20, right: 120, bottom: 20, left: 20 }}
      />
    </div>
  );
};

export default PieChartComponent;
