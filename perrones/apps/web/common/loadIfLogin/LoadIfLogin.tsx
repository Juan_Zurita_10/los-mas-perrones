import { signIn, signOut, useSession } from 'next-auth/react';
import React, { ReactNode, useEffect } from 'react';
import { usePathname, useSearchParams } from 'next/navigation';
import { CircularProgress, Grid } from '@mui/material';

export default function LoadIfLogin({
  children,
  role,
}: {
  children: ReactNode;
  role?: string[];
}) {
  const { data: session, status } = useSession();
  const pathname = usePathname();
  const searchParams = useSearchParams();

  useEffect(() => {
    if (
      status != 'loading' &&
      session &&
      session?.expires === 'RefreshAccessTokenError'
    ) {
      signOut({ callbackUrl: `${pathname}?${searchParams}`, redirect: true });
    }
  }, [session, status, pathname, searchParams]);

  if (status == 'loading') {
    return (
      <Grid container direction="column" justifyContent="center">
        <Grid container item justifyContent="center">
          <CircularProgress color="success"></CircularProgress>;
        </Grid>
      </Grid>
    );
  } else if (session) {
    if (
      role === null ||
      role === undefined ||
      role.some((element) => session?.user?.roles.includes(element))
    ) {
      return <>{children}</>;
    }
    return <>Error 401: Not allowed</>;
  }

  signIn('keycloak', {
    callbackUrl: `${pathname}?${searchParams}`,
    redirect: true,
  });
}
