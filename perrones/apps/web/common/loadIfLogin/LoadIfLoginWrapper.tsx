'use client';
import React, { ReactNode } from 'react';
import LoadIfLogin from './LoadIfLogin';
import { SessionProvider } from 'next-auth/react';

export default function LoadIfLoginWrapper({
  children,
  role,
}: {
  children: ReactNode;
  role?: string[];
}) {
  return (
    <SessionProvider>
      <LoadIfLogin role={role}>{children}</LoadIfLogin>
    </SessionProvider>
  );
}
