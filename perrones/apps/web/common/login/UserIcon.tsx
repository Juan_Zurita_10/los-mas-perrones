import {
  Avatar,
  Box,
  IconButton,
  Menu,
  MenuItem,
  Tooltip,
  Typography,
} from '@mui/material';
import { signOut } from 'next-auth/react';
import React, { MouseEvent, useState } from 'react';
export default function UserIcon({
  userName,
  userImg,
}: {
  userName: string | null | undefined;
  userImg: string | null | undefined;
}) {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const openMenu = (event: MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <Box alignItems="center" display="flex" flexDirection="column">
      <Tooltip title="Account Settings">
        <IconButton
          size="small"
          aria-controls={open ? 'account-menu' : undefined}
          aria-haspopup="true"
          aria-expanded={open ? 'true' : undefined}
          onClick={openMenu}
        >
          <Avatar
            sx={{
              width: 30,
              height: 30,
              borderRadius: '100%',
              border: '4px solid #419cbe',
            }}
            src={userImg || ''}
          ></Avatar>
        </IconButton>
      </Tooltip>

      <Menu
        open={open}
        anchorEl={anchorEl}
        id="account-menu"
        onClose={handleClose}
      >
        <Box>
          <Typography sx={{ fontSize: 16, fontWeight: 'bold', padding: 2 }}>
            {userName}
          </Typography>
        </Box>
        <MenuItem
          onClick={() => {
            signOut({
              callbackUrl: 'http://localhost:3000/',
              redirect: true,
            });
          }}
        >
          Salir
        </MenuItem>
      </Menu>
    </Box>
  );
}
