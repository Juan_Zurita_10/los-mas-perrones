import { signOut, useSession } from 'next-auth/react';
import { usePathname, useSearchParams } from 'next/navigation';
import { useEffect, useState } from 'react';
import React from 'react';
import UserIcon from './UserIcon';
import { Alert, CircularProgress, Snackbar } from '@mui/material';

export default function Login() {
  const { data: session, status } = useSession();
  const pathName = usePathname();
  const searchParams = useSearchParams();
  const [open, setOpen] = useState(true);
  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    if (
      status != 'loading' &&
      session &&
      session?.expires === 'RefreshAccessTokenError'
    ) {
      signOut({
        callbackUrl: `${pathName}?${searchParams}`,
        redirect: true,
      });
    }
  }, [session, status, pathName, searchParams]);

  if (status == 'loading') {
    return <CircularProgress></CircularProgress>;
  } else if (session) {
    return (
      <UserIcon
        userImg={session?.user?.image}
        userName={session?.user?.name}
      ></UserIcon>
    );
  } else {
    return (
      <Snackbar open={open} onClose={handleClose}>
        <Alert onClose={handleClose} severity="warning" sx={{ width: '100%' }}>
          Not logged in
        </Alert>
      </Snackbar>
    );
  }
}
