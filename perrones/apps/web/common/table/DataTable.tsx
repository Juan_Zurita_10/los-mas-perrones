import React from 'react';
import {
  Typography,
  Table,
  Grid,
  Paper,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from '@mui/material';
import BarChartTableCell from '../../components/BarChartTableCell';
import PieChartTableCell from '../../components/PieChartTableCell';
import LineChartTableCell from '../../components/LineChartTableCell';
import PetsCountTableCell from '../../components/PetsCountTableCell';

const DataTable = () => (
  <Grid item xs={12}>
    <Paper
      elevation={3}
      style={{
        position: 'relative',
        bottom: '0',
        left: '0',
        right: '0',
        paddingTop: '20px',
        paddingBottom: '20px',
        background: '#fafafa',
        width: '100%',
        maxHeight: '300',
      }}
    >
      <Typography padding={2} variant="h6" gutterBottom>
        Interpretation Of The Series (data)
      </Typography>
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>
                <strong>Bar Chart</strong>
              </TableCell>
              <TableCell>
                <strong>Pie Chart</strong>
              </TableCell>
              <TableCell>
                <strong>Line Chart</strong>
              </TableCell>
              <TableCell>
                <strong>Results</strong>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              <BarChartTableCell />
              <PieChartTableCell />
              <LineChartTableCell />
              <PetsCountTableCell />
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
    </Paper>
  </Grid>
);

export default DataTable;
