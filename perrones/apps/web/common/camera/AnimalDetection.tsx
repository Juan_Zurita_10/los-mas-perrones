'use client';
import { useEffect, useState } from 'react';
import { insertAnimal } from '../../services/AnimalService';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-expect-error
import ml5 from 'ml5';
import React from 'react';
import CustomList from '../../components/CustomList';
import AnimalModel from '../../types/AnimalData';

const AnimalDetection = () => {
  const [animalModels, setAnimalModels] = useState<Array<AnimalModel>>([]);
  let objectDetector: {
    detect: (
      arg0: unknown,
      arg1: (err: unknown, results: unknown) => void,
    ) => void;
  };
  let video: HTMLVideoElement | null = null;
  const make = async () => {
    video = await getVideo();

    if (video) {
      video.addEventListener('loadeddata', async () => {
        objectDetector = await ml5.objectDetector('cocossd', startDetecting);
      });
    }
  };

  useEffect(() => {
    make();

    return () => {
      if (video) {
        if (video.srcObject !== null && 'getTracks' in video.srcObject) {
          video.srcObject
            .getTracks()
            .forEach((track: { stop: () => unknown }) => track.stop());
        }
        video = null;
      }
    };
  }, [video]);

  const startDetecting = () => {
    detect();
  };

  const detect = () => {
    if (video) {
      objectDetector.detect(video, (err, results: unknown) => {
        if (err) {
          return;
        }

        if (Array.isArray(results)) {
          const objects = results.filter(
            (object: { label: string; confidence: number }) =>
              ['dog', 'cat', 'cow', 'horse'].includes(
                object.label.toLowerCase(),
              ) && object.confidence > 0.75,
          );

          if (objects.length > 0) {
            draw(objects);
          }
        }

        detect();
      });
    }
  };

  const draw = async (
    objects: Array<{
      label: string;
      confidence: number;
      x?: number;
      y?: number;
      width?: number;
      height?: number;
    }>,
  ) => {
    for (let i = 0; i < objects.length; i += 1) {
      const animal = objects[i];

      if (
        animal &&
        animal.x !== undefined &&
        animal.y !== undefined &&
        animal.width !== undefined &&
        animal.height !== undefined
      ) {
        if (animal.label !== 'dog' && animal.label !== 'cat') {
          animal.label = 'others';
        }

        const animalModel = {
          typeAnimal: animal.label,
          detectionTime: new Date(),
          score: animal.confidence,
          idCamera: getCameraId(),
        };

        setAnimalModels((previous) => [...previous, animalModel]);
        await insertAnimal(animalModel);
      }
    }
  };

  const getVideo = async () => {
    const existingVideo = document.querySelector('video');
    if (existingVideo) {
      return existingVideo;
    }

    const videoElement = document.createElement('video');
    const container = document.getElementById('animal-detection-container');
    if (container) {
      container.insertBefore(videoElement, container.firstChild);
    }

    try {
      videoElement.srcObject = await navigator.mediaDevices.getUserMedia({
        video: true,
      });
      await videoElement.play();
      return videoElement;
    } catch (error) {
      return null;
    }
  };

  const getCameraId = () => {
    if (video && video.srcObject) {
      const videoTracks = (video.srcObject as MediaStream).getVideoTracks();
      const definedTracks = videoTracks.filter(
        (track) => track !== undefined && track !== null,
      ) as MediaStreamTrack[];

      if (definedTracks.length > 0) {
        const firstTrack: MediaStreamTrack | undefined = definedTracks[0];

        if (firstTrack) {
          const { getSettings } = firstTrack;

          if (getSettings && typeof getSettings === 'function') {
            const settings = getSettings.call(firstTrack);
            if (settings.deviceId) {
              return settings.deviceId;
            }
          }
        }
      }
    }
    return 'unknownCamera';
  };

  return (
    <div style={{ alignSelf: 'flex-start' }}>
      <CustomList items={animalModels}></CustomList>
    </div>
  );
};
export default AnimalDetection;
