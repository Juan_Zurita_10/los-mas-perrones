import { Box, Grid, Tab, Tabs } from '@mui/material';
import React, { useState } from 'react';
import BarChartComponent from '../../components/BarChartComponent';
import PieChartComponent from '../../components/PieChartComponent';
import LineChartComponent from '../../components/LineChartComponent';

const Charts = () => {
  const [selectedTab, setSelectedTab] = useState('bar');
  return (
    <Box>
      <Tabs
        value={selectedTab}
        onChange={(e, newValue) => setSelectedTab(newValue)}
        centered
      >
        <Tab label="Bar Chart" value="bar"></Tab>
        <Tab label="Pie Chart" value="pie" />
        <Tab label="Line Chart" value="line" />
      </Tabs>
      {selectedTab === 'bar' && (
        <Grid container spacing={3} justifyContent="center" alignItems="center">
          <Grid item width="100%" padding={2}>
            <div
              style={{
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <BarChartComponent />
            </div>
          </Grid>
        </Grid>
      )}

      {selectedTab === 'pie' && (
        <Grid container spacing={3} justifyContent="center" alignItems="center">
          <Grid item width="100%" padding={2}>
            <div
              style={{
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <PieChartComponent />
            </div>
          </Grid>
        </Grid>
      )}
      {selectedTab === 'line' && (
        <Grid container spacing={3} justifyContent="center" alignItems="center">
          <Grid item width="80%" padding={2}>
            <div>
              <LineChartComponent />
            </div>
          </Grid>
        </Grid>
      )}
    </Box>
  );
};

export default Charts;
