'use client';

import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Login from '../login/Login';
import { ThemeProvider } from '@emotion/react';
import customTheme from '../../types/DefaultTheme';
import Image from 'next/image';
import { useSession } from 'next-auth/react';
import React from 'react';
import getHeaderItems from '../../types/HeaderItems';
import './HeaderStyles.css';

function NavItems() {
  const { data: session } = useSession();
  if (session) {
    const roles = session?.user?.roles;
    const navItems = getHeaderItems({ roles: roles });
    return (
      <Box width={'50%'} display={'flex'} justifyContent={'space-evenly'}>
        {navItems.map((elem, index) => (
          <Box display={'flex'} key={index} alignItems={'center'}>
            <Image
              src={elem.logoUrl}
              alt="nav icon"
              width={30}
              height={30}
              style={{ padding: 10 }}
            ></Image>
            <a href={elem.url} key={index}>
              <Typography key={index} style={{ fontSize: 20 }}>
                {elem.name}
              </Typography>
            </a>
          </Box>
        ))}
      </Box>
    );
  } else {
    return <Box>No session</Box>;
  }
}

export default function Header() {
  return (
    <ThemeProvider theme={customTheme}>
      <Box sx={{ flexGrow: 1 }}>
        <AppBar position="fixed">
          {' '}
          <Toolbar sx={{ display: 'flex', justifyContent: 'space-between' }}>
            <Box>
              <a href="http://localhost:3000">
                <Image
                  src="/perrones-icon.png"
                  width={45}
                  height={45}
                  alt="perrones icon"
                ></Image>
              </a>
            </Box>
              <NavItems></NavItems>
            <IconButton>
              <Login></Login>
            </IconButton>
          </Toolbar>
        </AppBar>
        <Box sx={{ pt: '64px' }}></Box>
      </Box>
    </ThemeProvider>
  );
}
