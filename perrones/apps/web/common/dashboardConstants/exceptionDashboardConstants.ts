export const INVALID_DATA_ERROR = 'Invalid data received from the server.';
export const FETCH_ERROR = 'There was a problem fetching the data: ';
