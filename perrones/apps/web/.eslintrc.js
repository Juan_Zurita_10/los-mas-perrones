module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 2021,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
      tsx: true,
    },
  },
  plugins: ['@typescript-eslint', 'react', 'jest', 'prettier'],
  extends: [
    'next',
    'plugin:@typescript-eslint/recommended',
    'plugin:react/recommended',
    'plugin:jest/recommended',
    'plugin:prettier/recommended',
  ],
  env: {
    browser: true,
    node: true,
    jest: true,
  },
  settings: {
    react: {
      version: 'detect',
    },
  },
  rules: {
    '@typescript-eslint/explicit-module-boundary-types': 'off',
    'react/prop-types': 'off',
    'prettier/prettier': ['error', { singleQuote: true, semi: true }],
    'no-console': 'warn',
    'no-unused-vars': 'warn',
    'no-undef': 'error',
    'prefer-const': 'error',
    'arrow-body-style': ['error', 'as-needed'],
    'prefer-template': 'error',
    'no-unreachable': 'error',
    'no-useless-constructor': 'error',
    'prefer-spread': 'error',
    'no-dupe-args': 'error',

    'prettier/prettier': [
      'error',
      { singleQuote: true, semi: true, usePrettierrc: true },
    ],
  },
};
