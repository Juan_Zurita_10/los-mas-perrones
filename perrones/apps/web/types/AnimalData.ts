type AnimalModel = {
  typeAnimal: string;
  detectionTime: Date;
  score: number;
  idCamera: string;
};

export default AnimalModel;
