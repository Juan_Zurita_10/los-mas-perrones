import { createTheme } from '@mui/material';

const customTheme = createTheme({
  palette: {
    primary: {
      main: '#BE6341',
    },
  },
});

export default customTheme;
