export type HeaderItems = {
  url: string;
  name: string;
  logoUrl: string;
};

const rootUrl = 'http://localhost:3000';

const cameraNavItems = {
  url: `${rootUrl}/cameras`,
  name: 'Cameras',
  logoUrl: '/cameras-icon.png',
};

const dashboardNavItem = {
  url: `${rootUrl}/dashboard`,
  name: 'Dashboard',
  logoUrl: '/dashboard-icon.png',
};

const aboutNavItems = {
  url: `${rootUrl}/information`,
  name: 'About',
  logoUrl: '/about-icon.png',
};

export default function getHeaderItems({
  roles,
}: {
  roles: string[];
}): HeaderItems[] {
  if (roles) {
    const navItems = [];
    if (roles.includes('veterinarian') || roles.includes('cameraMan')) {
      navItems.push(cameraNavItems);
    }

    if (roles.includes('cityPlanner') || roles.includes('veterinarian')) {
      navItems.push(dashboardNavItem);
    }

    navItems.push(aboutNavItems);

    return navItems;
  }
  return [];
}
