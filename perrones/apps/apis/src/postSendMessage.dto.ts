export class SendMessageDto {
    readonly to: string;
    readonly message: string;
  }
  