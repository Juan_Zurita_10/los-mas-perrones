import { PartialType } from '@nestjs/swagger';
import { PetRegisterDto } from './petRegister.dto';

export class UpdateRegisterDto extends PartialType(PetRegisterDto) {}
