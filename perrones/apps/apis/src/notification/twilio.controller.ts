
import { Controller, Post, Body } from '@nestjs/common';
import { TwilioService } from './twilio.service';
import { SendMessageDto } from '../postSendMessage.dto'
import { SendMessageDefNumDto } from '../postSendDefNumber.dto'
import {sendSMSByHandler } from './handlers/smsHandler'

@Controller('sms')
export class SMSController {
  constructor(private readonly twilioService: TwilioService) {}
@Post('/sendRepNum')
async sendSMSRecpNum(@Body() SendMessageDefNum: SendMessageDefNumDto) {
  try {
      const result = await this.twilioService.sendDefRecipientNumber(SendMessageDefNum.message)
      return { success: true, message: 'SMS sent successfully', result };
    } catch (error) {
      return { success: false, message: 'Failed to send SMS', error };
    }
  }
  @Post('/send')
  async sendSMS(@Body() sendMessageDto: SendMessageDto) {
  try {
      const result = await sendSMSByHandler(this.twilioService, sendMessageDto)
      return result;
    } catch (error) {
      return { success: false, message: 'Failed to send SMS', error };
    }
  }
}