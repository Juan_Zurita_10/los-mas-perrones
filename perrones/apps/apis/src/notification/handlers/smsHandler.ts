import { TwilioService } from '../twilio.service';
import { SendMessageDto } from '../../postSendMessage.dto'
class SmsHandler{
    public static twilioService: TwilioService;
    constructor(public readonly twilioService: TwilioService) {
        SmsHandler.twilioService = twilioService;
      }
      static async  sendSMSByHandlerTwilioInstance(sendMessageDto: SendMessageDto) {
        console.log(SmsHandler.twilioService)
        const { to, message } = sendMessageDto;
        try {
          const result = await SmsHandler.twilioService.sendSMS(to,message)
          return { success: true, message: 'SMS sent successfully', result };
        } catch (error) {
          return { success: false, message: 'Failed to send SMS', error };
        }
      }
}
async function sendSMSByHandler(twilioService: TwilioService, sendMessageDto: SendMessageDto) {
  const { to, message } = sendMessageDto;
  try {
    const result = await twilioService.sendSMS(to, message);
    return { success: true, message: 'SMS sent successfully', result };
  } catch (error) {
    return { success: false, message: 'Failed to send SMS', error };
  }
}

  
export { sendSMSByHandler, SmsHandler };
