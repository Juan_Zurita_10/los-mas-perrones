import { Test, TestingModule } from '@nestjs/testing';
import { SMSController } from './twilio.controller';
import { TwilioService } from './twilio.service';

describe('AppController', () => {
  let appController: SMSController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [SMSController],
      providers: [TwilioService],
    }).compile();

    appController = app.get<SMSController>(SMSController);
  });
});
