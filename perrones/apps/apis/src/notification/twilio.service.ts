import { Injectable } from '@nestjs/common';
import * as Twilio from 'twilio';
require('dotenv').config();

export class TwilioService {
  private readonly client: Twilio.Twilio;

  constructor() {
    this.client = Twilio(
        process.env.TWILIO_ACCOUNT_SID || '',
        process.env.TWILIO_AUTH_TOKEN || ''
    );
  }

  async sendSMS(to: string, body: string) {
    try {
      const message = await this.client.messages.create({
        body,
        to,
        from: process.env.TWILIO_PHONE_NUMBER || '',
      });
      return message;
    } catch (error) {
      console.error('Error sending SMS:', error);
      throw error;
    }
  }
  async sendDefRecipientNumber(body: string) {
    try {
      const message = await this.client.messages.create({
        body,
        to: process.env.RECIPIENT_NUMBER_DEMO || '',
        from: process.env.TWILIO_PHONE_NUMBER || '',
      });
      return message;
    } catch (error) {
      console.error('Error sending SMS:', error);
      throw error;
    }
  }
}

