# Twilio SMS Service using NestJS

This project demonstrates the integration of Twilio's SMS service into a NestJS application. It includes a service (`TwilioService`) responsible for sending SMS messages and a controller (`SMSController`) handling HTTP requests to send SMS messages via Twilio.

## Overview

The project structure contains the following files:

- `twilio.service.ts`: Contains the `TwilioService` class responsible for interfacing with Twilio's API to send SMS messages.
- `twilio.controller.ts`: Defines the `SMSController` responsible for handling HTTP POST requests to send SMS messages using the `TwilioService`.
- `postSendMessage.dto.ts`: Represents the data transfer object (DTO) for the payload expected when sending an SMS.

## Prerequisites

Before running the application, ensure you have set up the following:

1. **Twilio Account SID and Auth Token**: Obtain these credentials from your Twilio account dashboard.
2. **Twilio Phone Number**: You'll need a Twilio phone number to send SMS messages from.

## Installation

1. Install dependencies:

   ```bash
   npm install

# Twilio SMS Service using NestJS

This project demonstrates the integration of Twilio's SMS service into a NestJS application. It includes a service (`TwilioService`) responsible for sending SMS messages and a controller (`SMSController`) handling HTTP requests to send SMS messages via Twilio.

## Overview

The project structure contains the following files:

- `twilio.service.ts`: Contains the `TwilioService` class responsible for interfacing with Twilio's API to send SMS messages.
- `twilio.controller.ts`: Defines the `SMSController` responsible for handling HTTP POST requests to send SMS messages using the `TwilioService`.
- `postSendMessage.dto.ts`: Represents the data transfer object (DTO) for the payload expected when sending an SMS.

## Prerequisites

Before running the application, ensure you have set up the following:

1. **Twilio Account SID and Auth Token**: Obtain these credentials from your Twilio account dashboard.
2. **Twilio Phone Number**: You'll need a Twilio phone number to send SMS messages from.

## Installation

1. Install dependencies:

   ```bash
   npm install
   ```
Set up environment variables:
Create a .env file at the root of the project and add the following variables:
dotenv
TWILIO_ACCOUNT_SID=AC6aecbb5ec11294f65567c9668cf5e32e
TWILIO_AUTH_TOKEN=80a08e899b297bc0d80b67ff044ac046
TWILIO_PHONE_NUMBER=+14159694581
Usage

To use the application, follow these steps:

Start the server:
bash
Copy code
npm run start
Send an SMS:
Make a POST request to http://localhost:3002/sms/send with the following JSON payload:
json
{
  "to": "recipient_phone_number",
  "message": "Your_message_here"
}
Replace "recipient_phone_number" with the recipient's phone number such as:+59176476448 and "Your_message_here" with the desired message content.
Example using cURL:
bash


Input
The sendSMS endpoint expects a POST request with a JSON body containing:

"to": Recipient's phone number (string).
"message": Message content to be sent (string).


TWILIO_ACCOUNT_SID=AC3b92818631fa3c551b47d70a6b9771dd
TWILIO_AUTH_TOKEN=0d44057c927deae9b80419c7b03e4fbc
TWILIO_PHONE_NUMBER=+18156737523
RECIPIENT_NUMBER_DEMO=+59168576725

MJ_APIKEY_PUBLIC=21db5eae49879f472f09d9806633fb15
MJ_APIKEY_PRIVATE=6df91febbfed143d30467c0251b1cf4b
FRMEMMAIL= rosaleszuritajosesebastian@gmail.com
FRMNAME=LOS-MAS-PERRONES
SBJCT=[LOS-MAS-PERRONES-REPORT!]
RECIPIENT_MAIL=sebastian123456654@gmail.com