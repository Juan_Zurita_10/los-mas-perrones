import { Document } from 'mongoose';

export interface IPetRegister extends Document {
  readonly typeAnimal: string;
  readonly detectionTime: Date;
  readonly score: number;
  readonly idCamera: string;
}
