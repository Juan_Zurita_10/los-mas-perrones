import { IsNotEmpty, IsString, IsNumber, IsDateString } from 'class-validator';

export class PetRegisterDto {
  @IsString()
  @IsNotEmpty()
  readonly typeAnimal: string;

  @IsString()
  @IsNotEmpty()
  @IsDateString()
  readonly detectionTime: string;

  @IsNumber()
  readonly score: number;

  @IsString()
  @IsNotEmpty()
  readonly idCamera: string;
}
