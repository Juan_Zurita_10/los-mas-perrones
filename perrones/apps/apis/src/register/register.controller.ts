import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Post,
  Put,
  Res,
} from '@nestjs/common';
import { RegisterService } from './register.service';
import { PetRegisterDto } from 'src/petRegister.dto';
import { UpdateRegisterDto } from 'src/updatePetRegister.dto';

@Controller('petregister')
export class RegisterController {
  constructor(private readonly registryService: RegisterService) {}

  @Post()
  async createPetRegister(
    @Res() response,
    @Body() createRegisterDto: PetRegisterDto,
  ) {
    try {
      const newRegister =
        await this.registryService.createRegister(createRegisterDto);
      return response.status(HttpStatus.CREATED).json({
        message: 'Pet register created successfully',
        newRegister,
      });
    } catch (error) {
      return response.status(HttpStatus.BAD_REQUEST).json({
        statusCode: 400,
        message: 'Error: not created',
        error: 'Bad request',
      });
    }
  }

  @Put('/:id')
  async updatePetRegister(
    @Res() response,
    @Param('id') registerId: string,
    @Body() updateRegisterDto: UpdateRegisterDto,
  ) {
    try {
      const existingStudent = await this.registryService.updateRegister(
        registerId,
        updateRegisterDto,
      );

      return response.status(HttpStatus.OK).json({
        message: 'Pet register has been successfully updated',
        existingStudent,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get()
  async getPetRegisters(@Res() response) {
    try {
      const registerData = await this.registryService.getAllRegisters();
      return response.status(HttpStatus.OK).json({
        message: 'All pet registers found successfully',
        registerData,
      });
    } catch (error) {
      return response.status(error.status).json(error.response);
    }
  }

  @Delete('/:id')
  async deletePetRegister(@Res() response, @Param('id') registerId: string) {
    try {
      const deletedRegister =
        await this.registryService.deleteRegister(registerId);

      return response.status(HttpStatus.OK).json({
        message: 'Pet register deleted successfully',
        deletedRegister,
      });
    } catch (error) {
      return response.status(error.status).json(error.response);
    }
  }
}
