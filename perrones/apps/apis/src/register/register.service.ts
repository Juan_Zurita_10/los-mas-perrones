import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { NotFoundError } from 'rxjs';
import { PetRegisterDto } from 'src/petRegister.dto';
import { IPetRegister } from 'src/petRegister.interface';
import { UpdateRegisterDto } from 'src/updatePetRegister.dto';

@Injectable()
export class RegisterService {
  constructor(
    @InjectModel('Register') private registryModel: Model<IPetRegister>,
  ) {}

  async createRegister(
    createRegisterDto: PetRegisterDto,
  ): Promise<IPetRegister> {
    const newRegister = await new this.registryModel(createRegisterDto);
    return newRegister.save();
  }

  async updateRegister(
    animal: string,
    updateRegisterDto: UpdateRegisterDto,
  ): Promise<IPetRegister> {
    const existingRegister = await this.registryModel.findByIdAndUpdate(
      animal,
      updateRegisterDto,
      {
        new: true,
      },
    );

    if (!existingRegister) {
      throw new NotFoundError('Not Found');
    }
    return existingRegister;
  }

  async getAllRegisters(): Promise<IPetRegister[]> {
    const registerData = await this.registryModel.find();

    if (!registerData || registerData.length == 0) {
      throw new NotFoundException('Data not found');
    }

    return registerData;
  }

  async deleteRegister(registerId: string): Promise<IPetRegister> {
    const deletedStudent =
      await this.registryModel.findByIdAndDelete(registerId);

    if (!deletedStudent) {
      throw new NotFoundException('Student not found');
    }
    return deletedStudent;
  }
}
