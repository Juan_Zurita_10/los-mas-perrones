import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { v4 as uuidv4 } from 'uuid';
@Schema()
export class PetRegisterSchema {
  @Prop({ default: () => uuidv4() })
  idAnimal: string;

  @Prop()
  typeAnimal: string;

  @Prop()
  detectionTime: Date;

  @Prop()
  score: number;

  @Prop()
  idCamera: string;
}

export const RegisterSchema = SchemaFactory.createForClass(PetRegisterSchema);
