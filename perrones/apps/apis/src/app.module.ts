import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { RegisterSchema } from './petRegister.schema';
import { RegisterService } from './register/register.service';
import { RegisterController } from './register/register.controller';
import { SMSController } from './notification/twilio.controller';
import { TwilioService } from './notification/twilio.service';
import { JetmailController } from './mail/jetmail.controller';
import { JetmailService } from './mail/jetmail.service';


@Module({
  imports: [
    MongooseModule.forRoot(process.env.MONGO_URL, {
      dbName: 'ex1',
    }),
    MongooseModule.forFeature([{ name: 'Register', schema: RegisterSchema }]),
  ],
  controllers: [RegisterController,SMSController,JetmailController],
  providers: [RegisterService,TwilioService,JetmailService],
})
export class AppModule {}
