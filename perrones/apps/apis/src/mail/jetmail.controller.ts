import { Controller, Post, Body } from '@nestjs/common';
import { JetmailService } from './jetmail.service';

@Controller('jetmail')
export class JetmailController {
    constructor(private readonly jetmailService: JetmailService) {}

    @Post('sendemail')
    async sendEmail(
      @Body('textPart') textPart: string,
    ): Promise<any> {
      try {
        const response = await this.jetmailService.sendEmail(textPart);
        return response;
      } catch (error) {
        throw new Error(`Failed to send email: ${error.message}`);
      }
    }
}
