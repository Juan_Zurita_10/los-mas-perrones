import { Injectable } from '@nestjs/common';
const Mailjet = require('node-mailjet');
require('dotenv').config();

@Injectable()
export class JetmailService {


  async sendEmail(textPart: string): Promise<any> {
    const Mailjet = require('node-mailjet');
    const mailjet = Mailjet.apiConnect(
    process.env.MJ_APIKEY_PUBLIC,
    process.env.MJ_APIKEY_PRIVATE,
    );
    const request = mailjet.post('send', { version: 'v3.1' })
    .request({
      "Messages": [
        {
          "From": {
            "Email": process.env.FRMEMMAIL,
            "Name" : process.env.FRMNAME
          },
          "To": [
            {
              "Email": process.env.RECIPIENT_MAIL,
              "Name" : process.env.FRMNAME
            },
          ],
          "Subject": process.env.SBJCT,
          "TextPart": textPart,
        },
      ],
    });
    const response =  await request;
    return response.body;
  }
}
