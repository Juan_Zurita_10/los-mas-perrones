Mailjet is a cloud-based email-sending service that allows users to send marketing, transactional, and other types of emails through their API or SMTP relay. It provides various features like email templates, analytics, contact management, and more.

To use Mailjet's service, you typically need to:

1. **Sign up and get API credentials:** Register for a Mailjet account and obtain API keys (public and private) that authenticate your requests when using their API.

2. **Integrate Mailjet with your application:** This involves using Mailjet's API or SMTP server to send emails. The API integration would involve making HTTP requests to Mailjet's API endpoints, whereas the SMTP integration involves using Mailjet's SMTP server settings to send emails.

3. **Use the provided SDKs or libraries:** Mailjet offers SDKs in various programming languages to simplify integration with their services. These SDKs often encapsulate the HTTP requests and provide methods/functions to interact with Mailjet's services easily.

Returning to the code snippet you provided:

- The `JetmailController` in NestJS defines an endpoint at `/jetmail/sendemail` that expects a POST request with a `textPart` parameter in the request body.
- When a POST request is made to this endpoint, it triggers the `sendEmail` method in the `JetmailService`, passing the `textPart` parameter.
- Inside the `sendEmail` method of `JetmailService` (which is not shown), the implementation to communicate with Mailjet's API or SMTP service to send the email would reside.


.ENV vairbale needed /perrones/apps/apis/.env

MJ_APIKEY_PUBLIC=21db5eae49879f472f09d9806633fb15
MJ_APIKEY_PRIVATE=6df91febbfed143d30467c0251b1cf4b
FRMEMMAIL= rosaleszuritajosesebastian@gmail.com
FRMNAME=LOS-MAS-PERRONES
SBJCT=[LOS-MAS-PERRONES-REPORT!]
RECIPIENT_MAIL=sebastian123456654@gmail.com
