# Turborepo starter

This is an official starter Turborepo.

## Using this example

Run the following command:

```sh
npx create-turbo@latest
```

## What's inside?

This Turborepo includes the following packages/apps:

### Apps and Packages

- `docs`: a [Next.js](https://nextjs.org/) app
- `web`: another [Next.js](https://nextjs.org/) app
- `ui`: a stub React component library shared by both `web` and `docs` applications
- `eslint-config-custom`: `eslint` configurations (includes `eslint-config-next` and `eslint-config-prettier`)
- `tsconfig`: `tsconfig.json`s used throughout the monorepo

Each package/app is 100% [TypeScript](https://www.typescriptlang.org/).

### Utilities

This Turborepo has some additional tools already setup for you:

- [TypeScript](https://www.typescriptlang.org/) for static type checking
- [ESLint](https://eslint.org/) for code linting
- [Prettier](https://prettier.io) for code formatting

### Build

To build all apps and packages, run the following command:

```
cd my-turborepo
pnpm build
```

### Develop

To develop all apps and packages, run the following command:

```
cd my-turborepo
pnpm dev
```

### Remote Caching

Turborepo can use a technique known as [Remote Caching](https://turbo.build/repo/docs/core-concepts/remote-caching) to share cache artifacts across machines, enabling you to share build caches with your team and CI/CD pipelines.

By default, Turborepo will cache locally. To enable Remote Caching you will need an account with Vercel. If you don't have an account you can [create one](https://vercel.com/signup), then enter the following commands:

```
cd my-turborepo
npx turbo login
```

This will authenticate the Turborepo CLI with your [Vercel account](https://vercel.com/docs/concepts/personal-accounts/overview).

Next, you can link your Turborepo to your Remote Cache by running the following command from the root of your Turborepo:

```
npx turbo link
```

## Useful Links

Learn more about the power of Turborepo:

- [Tasks](https://turbo.build/repo/docs/core-concepts/monorepos/running-tasks)
- [Caching](https://turbo.build/repo/docs/core-concepts/caching)
- [Remote Caching](https://turbo.build/repo/docs/core-concepts/remote-caching)
- [Filtering](https://turbo.build/repo/docs/core-concepts/monorepos/filtering)
- [Configuration Options](https://turbo.build/repo/docs/reference/configuration)
- [CLI Usage](https://turbo.build/repo/docs/reference/command-line-reference)

# ESLint Usage Guide

### `npm run lint`

The command `npm run lint` executes ESLint according to the configurations defined in your project's ESLint setup. It's a convenient way to run ESLint scripts defined in your project's `package.json`.

### What it does:

- Runs ESLint based on the configurations set in your ESLint configuration file (`.eslintrc.js` or similar).
- Checks your code files for adherence to defined coding standards and style rules.
- Outputs errors, warnings, or suggestions according to your ESLint rules.
- Does not automatically fix issues; it only reports them.

### When to use:

- Use `npm run lint` to check your code for style violations, errors, and potential issues.
- Use it as part of your development workflow or as part of a CI/CD pipeline to ensure code quality.

## `npx eslint --fix path/file.js`

The command `npx eslint --fix path/file.js` is used to run ESLint with the `--fix` flag on a specific file or directory, attempting to automatically fix as many issues as possible.

### What it does:

- Runs ESLint with the `--fix` flag on the specified file or directory.
- Tries to automatically fix issues that ESLint can address based on the rules set in your ESLint configuration.
- Modifies the file in place if fixes are applied successfully.

### When to use:

- Use `npx eslint --fix` when you want ESLint to automatically fix as many issues as possible in a specific file or directory.
- Great for quickly addressing simple code style/formatting problems or minor issues without manually editing each occurrence.

## Conclusion

- `npm run lint` is used for checking code quality and style without automatically fixing issues.
- `npx eslint --fix path/file.js` is used to attempt automatic fixes on specific files or directories according to ESLint rules.
- Always review the changes made by the `--fix` option to ensure they align with your code style and functionality.

# Keycloak testing tutorial
1. Run `docker compose up -d` in the perrones/ folder to initialize keycloak. 
2. Once keycloak was initialized go to localhost:8080 and enter to the admin console.
3. Create a realm. 
4. Create a client with the next set up: 
 - Root URL: http://localhost:3000/
 - Home URL: http://localhost:3000/
 - Valid redirect URIs: http://localhost:3000/*
 - Valid post logout redirect URIs: http://localhost:3000/*
 - Web origins: http://localhost:3000/*
 - Client authentication: On 
Save the configuration of your client.
6. Create a realm role: Go to Real roles -> Create role. Roles defined are: 
 - "cameraMan": can access to camera.
 - "veterinarian" can access to camera and dashboards.
 - "cityPlanner": can access to dashboards.
 All people can access to about. 
5. Create a user: 
 - Go to User -> Create user. 
 - Click on your user to view the user details. 
 - To set a password go to Credentials -> Set password.
 - Go to Role mapping -> Assign roles, and set the roles you want the user to have. 
6. On perrones/apps/web/ create a .env.local file with this variables: 
```
CLIENT_ID={your-client-goes-here}
CLIENT_SECRET={the-client-secret-goes-here}
AUTH_ISSUER=http://localhost:8080/auth/realms/{your-realm}
NEXTAUTH_URL=http://localhost:3000/
END_SESSION_URL=http://localhost:8080/auth/realms/{your-realm}/protocol/openid-connect/logout
NEXTAUTH_SECRET=my-secret-goes-here-123
```
- You can get the client secret on keycloak. Go to Client details -> Credentials.
- Run `npm i` command in the perrones/ folder. 

Run `npm run dev` if everything is good, you can login and logout with the user you have configured. To view on perrones/dashboard only can be accessed for users with the role that are veterinarian and city planner. 

# GnuPG Password Storage

## Introduction
We will be using this tool in order to keep our development/production secrets safe and securily share them across the entire team. First we will explain how to grant a key access to the password store, then how to use the encrypted .env file, and finally how a teammate share an environment variable to the .env.gpg file:

## Steps
**Step 1: Give your key access to the pass store**
1. Create a gpg key
2. Check your new key
3. Encrypt your key
4. Share the encrypted key via Teams inbox.

```bash
    gpg --full-generate-key
    gpg --list-keys
    gpg --export --armor [your-key] > [your-name].asc
```

Now your key will be manually added to the password store. If you had trouble generating your key, follow the steps provided by the sample generation image.

**Step 2: Decrypting the .env.gpg file**
1. Move to your .env.gpg location
2. Decrypt it
3. Add it to the projects root path

```bash
    gpg -d .env.gpg > .env
```

**Step 3: Sharing new secrets**
In order to edit or add new environment variables to the .env.gpg send them via Teams inbox as well and they will be added.

## Documentation and Guides:
1. GnuPG, pass and environment variables: https://www.kunxi.org/2023/02/pass-gnupg-and-environment-variables/
2. Sharing passwords with Git, GPG and pass https://inconclusive.medium.com/sharing-passwords-with-git-gpg-and-pass-628c2db2a9de 

# Tensorflow Coco Dataset

1. Execute the commands:

```bash
npm i 
npm run dev
```
2. Move to: http://localhost:3000/cameras
3. Provide an adequate pet picture (dog or cat for instance)
5. Press "try now" in the path GET /petregister
6. Observe the new pet detections.

# Retry Pattern

This TypeScript module implements a combination of Exponential Backoff and Circuit Breaker for managing HTTP requests using Axios. The utilization of these strategies aims to enhance the resilience and reliability of the application when interacting with external services.

## Exponential Backoff

**What is Exponential Backoff?**

Exponential Backoff is a strategy that involves gradually increasing the time between retries for a failed operation. In this module, it is employed to handle scenarios where a temporary failure in an HTTP request occurs. The delay between retries follows an exponential pattern, providing a balance between retry attempts and allowing the system to recover from transient issues.

**Why Exponential Backoff?**

- **Resilience**: Helps the application to gracefully handle intermittent failures in the network.

- **Reduced Load**: Prevents overwhelming a service with a high number of rapid retries during temporary failures.

## Circuit Breaker

**What is Circuit Breaker?**

The Circuit Breaker pattern is a mechanism to detect and prevent continuous requests to a failing service. Once a certain threshold of failures is reached, the circuit is "opened," and further requests are temporarily halted. This prevents the system from repeatedly attempting requests that are likely to fail.

**Why Circuit Breaker?**

- **Fail-Fast**: Quickly detects and responds to ongoing failures, minimizing the impact on the application.

- **Preventive Measures**: Avoids overloading a service that is experiencing persistent issues, allowing it time to recover.

## Implementation in Endpoints

The combination of Exponential Backoff and Circuit Breaker in HTTP endpoints is crucial for the following reasons:

1. **Resilience Against Transient Failures**: The Exponential Backoff component ensures that, in case of temporary issues, the system retries requests with increasing intervals, providing a higher chance of success without overloading the service.

2. **Prevention of Continuous Retries**: The Circuit Breaker adds an additional layer of protection by temporarily halting retries when a certain failure threshold is reached. This prevents continuous attempts during prolonged service outages.

3. **Enhanced Reliability**: By implementing these strategies, the application becomes more resilient and reliable, adapting to varying conditions in the external services it interacts with.

In summary, the combination of Exponential Backoff and Circuit Breaker provides a robust mechanism for handling HTTP requests, contributing to a more resilient and reliable overall system architecture.

## How to test
note: wait a moment for see delay message

#### 1. Run the project (npm i - npm run dev).
#### 2. kill the DB (stop mongo).
#### 2. Run go to the dashboard page. 
#### 3. open the browser inspect.  
#### 4. View the console for delay response